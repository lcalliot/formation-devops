USE `chaos`;

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city`
(
    `id`                 int(11)      NOT NULL AUTO_INCREMENT,
    `name`               varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city`
    DISABLE KEYS */;
INSERT INTO `city`
VALUES (1, 'Bordeaux'),
       (2, 'Paris'),
       (3, 'Strasbourg'),
       (4, 'Lyon'),
       (5, 'Marseille');
/*!40000 ALTER TABLE `city`
    ENABLE KEYS */;
UNLOCK TABLES;
